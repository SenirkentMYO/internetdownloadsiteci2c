﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="kayit.aspx.cs" Inherits="download_sitesi.kayit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 25%;
            height: 287px;
        }
        .auto-style2 {
            width: 4px;
            text-align: center;
        }
        .auto-style3 {
            width: 168px;
            text-align: center;
            font-weight: 700;
        }
        #form1 {
            background-color: #FB8507;
        }
        .auto-style4 {
            width: 168px;
            text-align: center;
            font-weight: 700;
            background-color: #FB8507;
        }
        .auto-style5 {
            color: #FFFFFF;
        }
    </style>
</head>
<body style="height: 263px; background-color: #FB8507">
    <form id="form1" runat="server">
  
        <table align="center" border="1" class="auto-style1">
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label5" runat="server" CssClass="auto-style5" Text="Ad"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label6" runat="server" CssClass="auto-style5" Text="Soyad"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label1" runat="server" CssClass="auto-style5" Text="Kullanıcı Adı"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label2" runat="server" CssClass="auto-style5" Text="Parola"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label3" runat="server" CssClass="auto-style5" Text="Parola Tekrar"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style4">
                    <asp:Label ID="Label4" runat="server" CssClass="auto-style5" Text="E-mail"></asp:Label>
                </td>
                <td class="auto-style2">
                    <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    <asp:Button ID="Button1" runat="server" Text="Kayıt Ol" />
                </td>
                <td class="auto-style2">
                    <asp:Button ID="Button2" runat="server" style="text-align: center" Text="Temizle" />
                </td>
            </tr>
        </table>
  
    </form>
</body>
</html>
